import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtQuick.Controls
import QtWebView


Window {
    width: 1024
    height: 768
    visible: true
    title: qsTr("Jetank Remote")

    property string robotAddress: "http://192.168.137.63:8000";

    function rad(degrees)
    {
      var pi = Math.PI;
      return degrees * (pi/180);
    }

    function setServoAngle(servo, value)
    {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", "%1/servo/%2/%3".arg(robotAddress, servo, value))
        xhr.send()
    }
    function setMotorPower(value)
    {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", "%1/motor/power/%2".arg(robotAddress, value))
        xhr.send()
    }
    function setMotorBalance(value)
    {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", "%1/motor/balance/%2".arg(robotAddress, value))
        xhr.send()
    }

    function setFollow(value)
    {
        var command = value ? "startfollow" : "stopfollow";
        const xhr = new XMLHttpRequest()
        xhr.open("GET", "%1/%2".arg(robotAddress, command))
        xhr.send()
    }

    property int cameraAngle: 0;
    property int armAngle: -90;
    property int arm2Angle: 90;
    property int clawAngle: 30;

    property int cameraLength: 10;
    property int armLength: 30;
    property int arm2Length: 20;
    property int clawLength: 10;

    property int rowHeight: 200

    color: "gray"

    ColumnLayout
    {
        anchors.fill: parent
        WebView {
            Layout.fillHeight: true
            Layout.fillWidth: true
            url: "http://192.168.137.63:8000/camera"
        }
        RowLayout {
            Layout.fillWidth: true

            Item {
                Layout.fillWidth: true
            }

            Dial {
                width: rowHeight
                height: rowHeight
                from:-100
                to:100
                onMoved: {
                    setServoAngle(1, value)
                }
                Text {
                    text: "Rotation"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Dial {
                width: rowHeight
                height: rowHeight
                from:-100
                to:100
                onMoved: {
                    armAngle = value - 90;
                    canvas.requestPaint();
                    setServoAngle(2, value)
                }
                Text {
                    text: "Lower Arm"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Dial {
                width: rowHeight
                height: rowHeight
                from:-100
                to:100
                onMoved: {
                    arm2Angle = value + 90;
                    canvas.requestPaint();
                    setServoAngle(3, -value)
                }
                Text {
                    text: "Upper Arm"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Slider {
                height: rowHeight
                orientation: Qt.Vertical
                from: 0
                to: 70
                onMoved: {
                    clawAngle = 70-value;
                    canvas.requestPaint();
                    setServoAngle(4, -value)
                }
                Text {
                    text: "Claw"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Dial {
                width: rowHeight
                height: rowHeight
                from:-50
                to:50
                onMoved: {
                    cameraAngle = value;
                    canvas.requestPaint();
                    setServoAngle(5, value)
                }
                Text {
                    text: "Camera"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Item{
                width: rowHeight
                height: rowHeight

                Canvas {
                    id: canvas
                    width: 100
                    height: 100
                    transformOrigin: Item.TopLeft
                    scale: Math.min(parent.height/height, parent.width/width)

                    onPaint: {
                        var armRoot = [50, 90];
                        var armPos = [armRoot[0] + Math.cos(rad(armAngle))*armLength, armRoot[1] + Math.sin(rad(armAngle)) * armLength]
                        var arm2Pos = [armPos[0] + Math.cos(rad(armAngle+arm2Angle))*arm2Length, armPos[1] + Math.sin(rad(armAngle+arm2Angle)) * arm2Length]

                        var clawPos1 = [arm2Pos[0] + Math.cos(rad(armAngle+arm2Angle+clawAngle))*clawLength, arm2Pos[1] + Math.sin(rad(armAngle+arm2Angle+clawAngle)) * clawLength]
                        var clawPos2 = [arm2Pos[0] + Math.cos(rad(armAngle+arm2Angle-clawAngle))*clawLength, arm2Pos[1] + Math.sin(rad(armAngle+arm2Angle-clawAngle)) * clawLength]


                        var cameraRoot = [50, 90];
                        var cameraPos = [cameraRoot[0] + Math.cos(rad(cameraAngle))*cameraLength, cameraRoot[1] + Math.sin(rad(cameraAngle)) * cameraLength]

                        var ctx = getContext("2d");
                        ctx.fillStyle = Qt.rgba(1, 1, 1, 1);
                        ctx.fillRect(0, 0, 100, 100);

                        ctx.fillStyle = Qt.rgba(0, 0, 0, 1);


                        ctx.strokeStyle = Qt.rgba(0, 0, 0, 1);
                        ctx.lineWidth = 2;
                        ctx.beginPath();

                        ctx.moveTo(armRoot[0], armRoot[1])
                        ctx.lineTo(armPos[0], armPos[1])
                        ctx.lineTo(arm2Pos[0], arm2Pos[1])
                        ctx.lineTo(clawPos1[0], clawPos1[1])
                        ctx.moveTo(arm2Pos[0], arm2Pos[1])
                        ctx.lineTo(clawPos2[0], clawPos2[1])

                        ctx.moveTo(cameraRoot[0], cameraRoot[1])
                        ctx.lineTo(cameraPos[0], cameraPos[1])

                        ctx.stroke();

                    }
                    MouseArea {
                        anchors.fill: parent
                        onPressed: {  }
                        onReleased: {  }
                    }
                }
            }

            Dial {
                width: rowHeight
                height: rowHeight
                from:-1
                to:1
                id: motorBalanceDial
                onMoved: {
                    setMotorBalance(value)
                }
                Text {
                    text: "Balance"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Slider {
                height: rowHeight
                orientation: Qt.Vertical
                from: -1
                to: 1
                id: motorPowerSlider
                onMoved: {
                    setMotorPower(value)
                }
                Text {
                    text: "Power"
                    width: parent.width
                    anchors.top: parent.bottom
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Column {
                RadioButton {
                    text:"on"
                    onClicked: {
                        setFollow(true)
                    }
                }
                RadioButton {
                    id: offButton
                    text:"off"
                    checked: true
                    onClicked: {
                        setFollow(false)
                    }
                }
                Text {
                    text: "Follow line"
                    horizontalAlignment:Text.AlignHCenter
                }
            }

            Button {
                text: "STOP"
                width: rowHeight
                height: rowHeight
                onClicked: {
                    motorBalanceDial.value = 0
                    motorPowerSlider.value = 0
                    setMotorBalance(0)
                    setMotorPower(0)

                    offButton.checked = true;
                    setFollow(false)
                }
            }

            Item {
                Layout.fillWidth: true
            }
        }

    }
}
