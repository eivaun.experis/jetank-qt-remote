import cv2
import threading
from jetbot import Camera

class CameraStream:
    def __init__(self, width=1920, height=1080):
        # Start the camera
        self.camera = Camera.instance(width=width, height=height)
        
        # Use locks for thread-safe viewing of frames in multiple browsers
        self.thread_lock = threading.Lock()

        # Image frame sent to the Flask object
        self.video_frame = None
    
    def stop(self):
        self.camera.stop()
        
    def capture(self):
        frame = self.camera.value
        self.video_frame = frame.copy()
        return self.video_frame

    # Image stream response
    def encode_frame(self):
        while True:
            # Acquire thread_lock to access the video_frame object
            with self.thread_lock:
                if self.video_frame is None:
                    continue
                return_key, encoded_image = cv2.imencode(".jpg", self.video_frame)
                if not return_key:
                    continue
            # Output image as a byte array
            yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
                bytearray(encoded_image) + b'\r\n')