import cv2
import numpy as np
from jetbot import Robot
from SCSCtrl import TTLServo

# --Constants--
processing_resolution = (300, 300)
# Color range for white
color_upper = np.array([180, 50, 255])
color_lower = np.array([0, 0, 200])
# Max and min speed when turning
max_speed = 1
min_speed = 0.5

# Parameters for drawing the driving guide lines
line_color = (0, 255, 0)
line_thickness = 4

"""
Takes in an array of points, and orders them based on relative location.
Borrowed from https://github.com/tprlab/pitanq-dev/tree/master/selfdrive/follow_line
params: box (np.array) - array of points
returns: np.array - points ordered like [top_l, top_r, btm_r, btm_l]
"""
def order_box(box):
    srt = np.argsort(box[:, 1])
    btm1 = box[srt[0]]
    btm2 = box[srt[1]]

    top1 = box[srt[2]]
    top2 = box[srt[3]]

    bc = btm1[0] < btm2[0]
    btm_l = btm1 if bc else btm2
    btm_r = btm2 if bc else btm1

    tc = top1[0] < top2[0]
    top_l = top1 if tc else top2
    top_r = top2 if tc else top1
    
    return np.array([top_l, top_r, btm_r, btm_l])
    
    
"""
Will track a white line and follow it
params: input_frame (np.array) - frame to process
returns: np.array - processed frame for visualization
"""
def track_line(input_frame, robot):
    processing_frame = cv2.resize(input_frame, processing_resolution)
    # Block front of camera to avoid false positives
    # Needed to make up for the insanve FOV of this lens
    cv2.rectangle(processing_frame, (0,0), (300,75), (0,0,0), -1)
    
    # Create mask of frame based on color range
    hsv = cv2.cvtColor(processing_frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, color_lower, color_upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    
    # Obtain the conformed area contour.
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    
    # If a line was found, move the robot
    if len(cnts) > 0:
        c = max(cnts, key=cv2.contourArea)      
        
        # Get corners of rectangle containing most of the line
        rect = cv2.minAreaRect(c)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        
        # Get point at top and bottom to create pseudo-vector
        box = order_box(box)
        tip = [min(box[1][0],box[0][0]) + abs(box[1][0]-box[0][0])//2, min(box[1][1],box[0][1]) + abs(box[1][1]-box[0][1])//2]
        base = [min(box[3][0],box[2][0]) + abs(box[3][0]-box[2][0])//2, min(box[3][1],box[2][1]) + abs(box[3][1]-box[2][1])//2]
        
        # Adjust motorspeeds according to angle of pseudo-vector
        if tip[0]<130:
            f = (130-tip[0])/130
            s = min_speed + (max_speed-min_speed)*f
            robot.set_motors((1-s)*(-1 if tip[1] > 200 else 1), s)
        elif tip[0]>170:
            f = (300-tip[0])/130
            s = min_speed + (max_speed-min_speed)*f
            robot.set_motors(s, (1-s)*(-1 if tip[1] > 200 else 1))
        else:
            robot.set_motors(0.5, 0.5)
        
        frame_h, frame_w, _ = input_frame.shape
        box[:,0] *= frame_w // processing_resolution[0]
        box[:,1] *= frame_h // processing_resolution[1]
        
        # Draw line on frame to visualize
        cv2.drawContours(input_frame, [box], 0, line_color, line_thickness)
    
    # If a line was NOT found, stop the robot
    else:
        robot.stop()
        
    # Return frame with visuals
    return input_frame